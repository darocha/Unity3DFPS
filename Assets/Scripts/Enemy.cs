﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    
    public int EnemyHealty = 10;
    
	// Update is called once per frame
	void Update () {
        if (EnemyHealty <= 0) {
            Destroy(gameObject);
        }
    }

    void DeductPoints(int DamageAmount)
    {
        EnemyHealty -= DamageAmount;
    }
}
